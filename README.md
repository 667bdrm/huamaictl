## huamaictl

PTZ control tool and generic SDK for Huamai PTZ camera 5508N-W-IR (eye10000.com)

Example item:
http://www.aliexpress.com/item/New-IR-wireless-ip-camera-with-P2P-H-264-PTZ-WIFI-SD-Slot-Network-camera-Free/782606869.html

Maybe Wanscam modification

## Usage:

ipcptz.pl --host <ip> --port <port> --authserial <serial> --authtime <time> --authtoken <token> --direction <up|down|left|right|patrol|zoomin|zoomout> --speed <speed>

authtime and authtoken must be grabbed from the original Windows desktop application traffic (<Authentication></Authentication> and <Time></Time>

Don't know right now how to calculate authtoken (custom MD5 hash based on "2huamai8D" salt), but constant time+token pair works perfect. See hm::CHmRequest_Certification::BuildMD5()


## Author and License

Copyright (C) 2016-2020 667bdrm
Dual licensed under GNU General Public License 2 and commercial license
Commercial license available by request
